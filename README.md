# ~/ABRÜPT/C. JEANNEY/MÜNRI/*

La [page de ce livre](https://abrupt.ch/c-jeanney/munri/) sur le réseau.

## Sur le livre

Je suis une Méthode Üniverselle pour Ne Rien Inventer. Prière de me manier avec précaution.

## Sur l'autrice

C. Jeanney est autrice, parfois traductrice, souvent artiste visuelle. Elle fait ce qu’elle a à faire, et ce qu’elle a fait parle pour elle. Pas besoin de chercher beaucoup plus.

Et ce qu’elle a fait ou fait ou fera se trouve notamment sur son site [tentatives](http://christinejeanney.net/). [La suite a six minutes](https://lasuiteasixminutes.com/) y fait suite. La [maison\[s\]témoin](http://www.maisonstemoin.fr/author/cjeanney/) aussi.

Il est possible de la suivre sur [facebook](https://www.facebook.com/christine.jeanney.16) et [twitter](https://twitter.com/cjeanney). La liste de ses ouvrages se trouve sur [la page bibliographique de son site](http://christinejeanney.net/spip.php?article939).

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
