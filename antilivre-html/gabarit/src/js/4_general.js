// General script
// Copy Data URI images to href
window.onload = function photographies() {
  var copyto = document.querySelectorAll('.photo__lien');
  var copy = document.querySelectorAll('.photo__image');
  for (var i = 0; i < copyto.length; i++) {
    var url = copy[i].getAttribute("src");
    copyto[i].href = url;
  }
}

// When page loaded
window.addEventListener('load', function () {
  document.body.classList.add('loaded');
});

//Fancybox reorder
//
// sort array of objects by a property
var sortObjectsBy = function(field, reverse, primer) {
  var key = primer ? function(x) {
    return primer(x[field])
  } : function(x) {
    return x[field]
  };
  reverse = !reverse ? 1 : -1;
  return function(a, b) {
    return a = key(a),
      b = key(b),
      reverse * ((a > b) - (b > a));
  }
}

// Custom click handler
$('.photo__lien').on('click', function(e) {
  var fancyElements = [];

  e.preventDefault();

  // create array of fancyBox objects
  $('.photo__lien').each(function( index ) {
    var el = $(this).get(0),
        order = $(this).data('order');

    fancyElements.push({
      src     : el.href,
      thumb   : el.href,
      caption : el.title,
      order   : order
    });
  });

  // sort the fancybox array of objects by the "order" property
  fancyElements.sort(sortObjectsBy("order", false, function(a) {
    return a;
  }));

  // launch fancybox programmatically
  $.fancybox.open(
    fancyElements,
    {
      // custom options
      loop: true
    },
    $(this).data('order') - 1 // start gallery from the clicked element
  );

});


//
// scroll
// Source : https://codepen.io/rebosante/pen/eENYBv

const elemGauche = document.querySelector(".left");
const elemDroite = document.querySelector(".right");

const menu = document.querySelector('.menu');
const info = document.querySelector('.info');

function randomNb(min, max, div) {
  let nb = Math.floor(Math.random() * (max - min + 1) + min);

  if (div === "odd" && nb % 2 === 0) {
    nb -= 1;
  } else if (div === "even" && nb % 2 !== 0) {
    nb += 1;
  }
  return nb;
}

document.querySelector('.shuffle').addEventListener("click", function(e) {
  e.preventDefault();
  info.classList.remove('show');

  const firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
  const element = document.querySelector('#photo' + randomNb(1,60,"odd"));
  const element2 = document.querySelector('#photo' + randomNb(1,60,"even"));

  if(firefox)
  {
    element.scrollIntoView({behavior: "smooth", block: "center", inline: "center"});
    element2.scrollIntoView({behavior: "smooth", block: "center", inline: "center"});
  } else {
    element.scrollIntoView({block: "center", inline: "center"});
    element2.scrollIntoView({block: "center", inline: "center"});
  }

});

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

menu.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.toggle('show');
});
